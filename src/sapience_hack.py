""" Sapience Hacker """
import pyautogui as gui
from time import sleep
import time
from random import SystemRandom
from tasks.desktop import backToDesktop
from tasks.eclipse import runEclipse
from tasks.sql_developer import runSqlDeveloper
from tasks.git import runGitBash
from tasks.notepad import runNotePad
import signal
import sys
from datetime import date
import json

def signal_handler(sig, frame):
    today = date.today()
    print('You pressed Ctrl+C!')
    x = 0.0
    data = {}
    with open('tracking.json', 'r') as f:
        data = json.load(f)
        x = data.get(today.strftime("%d/%m/%Y"), 0.0)
        from util import total_time
        x += total_time
    with open('tracking.json', 'w') as f:
        data[today.strftime("%d/%m/%Y")] = x
        json.dump(data, f, indent=4, sort_keys=True)
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)
print('Press Ctrl+C to exit')

def start():
    while True:
        sleep(2)
        eclipseInit = True
        sqlInit = True
        padInit = True

        x = SystemRandom().randint(0, 20)
        backToDesktop()
        if x < 5 and x >= 0:
            runSqlDeveloper(sqlInit)
            sleep(SystemRandom().randint(0, 10))
        elif x >= 5 and x < 10:
            runEclipse(eclipseInit)
        elif x >= 10 and x < 15:
            runNotePad(padInit)
            sleep(SystemRandom().randint(0, 10))
        elif x >= 15 and x < 20:
            runGitBash(True)
        backToDesktop()
start()
