import time

total_time = 0

def timeit(method):
    def timed(*args, **kw):
        global total_time
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        total_time += (te - ts) / 60
        print('%r  %2.2f Minutes' % \
                (method.__name__, ((te - ts) / 60)))
        print(f'total time spent on automation {total_time} Minutes')
        return result
    return timed

BIG = 1
SMALL = 2

def scale(x, y, display = 2):
    x = int(x/1.171303074)
    y = int(y/1.17875)
    return (x, y)
