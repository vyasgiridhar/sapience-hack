from tasks import gui
from time import sleep
from util import timeit
from random import SystemRandom

bash_scripts = ['data/lorem_ipsum_2', 'data/lorem_ipsum_3', 'data/lorem_ipsum_4']

def choose_bash():
    with open(SystemRandom().choice(bash_scripts)) as f:
        bash_data = f.readlines()
        return bash_data


def run_git_bash(initialize: bool = False):
    if initialize:
        sleep(2)
        gui.click(356, 751)

    sleep(2)

    for line in choose_bash():
        line = "echo \"" + line + "\"" 
        gui.write(line, SystemRandom().uniform(0, 0.5))

@timeit
def runGitBash(gitInit):
    sleep(10)
    run_git_bash(gitInit)
