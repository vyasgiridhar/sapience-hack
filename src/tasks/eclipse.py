from time import sleep
import time
from tasks import gui, randomMovement, check_if_in_screen
from typing import List
from random import SystemRandom
from util import timeit, scale
from tasks.notepad import choose_ipsum_lorem


def eclipse(initialize: bool = False):
    if initialize:
        sleep(2)
        #Eclipse icon position
        gui.click(307, 750)
    sleep(10)

    if check_if_in_screen("data/eclipse.png") == False:
        return

    gui.click(136, 209)
    gui.drag(100, 100, duration=2)

    gui.hotkey('ctrl', 'c')

    sleep(5)
    gui.scroll(SystemRandom().randint(0, 20) - 40)

    sleep(4)
    gui.hotkey('ctrl', 'v')

    for line in choose_ipsum_lorem():
        gui.write(line, interval=SystemRandom().uniform(0, 0.5))

    randomMovement()

@timeit
def runEclipse(eclipseInit):
    i = 0
    while i < 5:
        eclipse(eclipseInit)
        eclipseInit = False
        if SystemRandom().randint(0, 10) > 6:
            return
        i += 1
