from time import sleep
import time
from tasks import gui, randomMovement, check_if_in_screen
from typing import List
from random import SystemRandom
from util import timeit, scale


def notepad(initialize: bool = False, ipsum_lorem: List[str] = []):
    if initialize:
        sleep(2)
        #Notepad icon position
        gui.click(267, 754)
    else:
        if check_if_in_screen("data/notepad.png") == False:
            return
        # Open a new tab
        gui.doubleClick(469, 78)

    sleep(5)
    for line in ipsum_lorem:
        gui.write(line, interval=SystemRandom().uniform(0, 0.5))
    sleep(5)

    randomMovement()


def choose_ipsum_lorem() -> List[str]:
    file_names = ["data/ipsum_lorem", "data/lorem_ipsum_2", "data/lorem_ipsum_3", "data/lorem_ipsum_4", "data/ipsum_lorem_4"]
    with open(SystemRandom().choice(file_names), 'r') as f:
        ipsum_lorem = f.readlines()
        return ipsum_lorem


@timeit
def runNotePad(padInit):
    notepad(padInit, choose_ipsum_lorem())
    padInit = False
    randomMovement()
