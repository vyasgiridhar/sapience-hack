from time import sleep
import time
from tasks import gui, randomMovement, check_if_in_screen
from typing import List
from random import SystemRandom
from util import timeit, scale

queries = ["select * from account", "select * from personaccount",
           "select * from tokensaudit", "select * from wsaudit",
           "select * from dual",
           "select * from programparamvalue where programid=663",
           "select * from person where personid in (select personid from personaccount where accountid in (select accountid from account where programid=663))"]

def sqlDeveloper(initialize: bool = False):

    if initialize:
        sleep(2)
        #Sql Developer icon position
        gui.click(219, 754)
        sleep(10)

        # Create OLTP Connection

        gui.click(16, 210)
        sleep(3)

        # OLTP dev connection tab
        gui.click(370, 90)

    if check_if_in_screen("data/sql_developer.png") == False:
        return

    # SQL Editor
    gui.click(685, 696)

    sleep(2)

    # Clear the Worksheet
    gui.hotkey('ctrl', 'a')
    gui.press("backspace")

    # Run one of those queries
    gui.write(SystemRandom().choice(queries), interval=SystemRandom().uniform(0, 0.5))

    gui.press('f9')

    randomMovement()

@timeit
def runSqlDeveloper(sqlInit):
    i = 0
    while i < 20:
        sqlDeveloper(sqlInit)
        sqlInit = False
        if SystemRandom().randint(0, 10) > 8:
            return
        i += 1
        randomMovement()
